package main_pack;

import pub_pack.Pub1;
import pub_pack.Pub1Child1;

//��package : default VS public
public class AccessTest {

	public static void main(String[] args) {
		
		System.out.println("#### public class #################");
		Pub1 p = new Pub1();
//		System.out.println(p.paramPV);
		System.out.println(p.paramPB);
//		System.out.println(p.paramDF);
//		System.out.println(p.paramPT);
		System.out.println(p.pub());
		
		System.out.println("$$ default class $$$$$$$$$$$$$$");
//		Def1 d = new Def1();
		
		System.out.println("~~~~~~~~~~ extends ~~~~~~~~~~~~~~~~~~~~~~");
		Pub1Child1 pc = new Pub1Child1();
		pc.showFatherContent();
		
	}

}
