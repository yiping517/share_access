package def_pack;

//�Ppackage : private
class DefInPub{
	void defInPub() {
		Def1 d = new Def1();
		System.out.print(d.paramDF);
		System.out.print(d.paramPB);
//		System.out.print(d.paramPV);
		System.out.print(d.paramPT);
	}
}

class Def1 {
	
	private String paramPV = "def1 : PV";
	public String paramPB = "def1 : PB";
	protected String paramPT = "def1 : protected";
	String paramDF = "def1 : default";
	
	public String pub() {
		return "def1 : pub()";
	}
}

